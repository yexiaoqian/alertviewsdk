# RCAlertView

[![CI Status](https://img.shields.io/travis/yexiaoqian/RCAlertView.svg?style=flat)](https://travis-ci.org/yexiaoqian/RCAlertView)
[![Version](https://img.shields.io/cocoapods/v/RCAlertView.svg?style=flat)](https://cocoapods.org/pods/RCAlertView)
[![License](https://img.shields.io/cocoapods/l/RCAlertView.svg?style=flat)](https://cocoapods.org/pods/RCAlertView)
[![Platform](https://img.shields.io/cocoapods/p/RCAlertView.svg?style=flat)](https://cocoapods.org/pods/RCAlertView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RCAlertView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RCAlertView'
```

## Author

yexiaoqian, 441745749@qq.com

## License

RCAlertView is available under the MIT license. See the LICENSE file for more info.
